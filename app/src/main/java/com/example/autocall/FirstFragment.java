package com.example.autocall;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.autocall.databinding.FragmentFirstBinding;
import com.google.android.material.snackbar.Snackbar;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;
    private Boolean isRepeating = false;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Handler handler = new Handler();

        binding.buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRepeating = !isRepeating;

                if (isRepeating) {
                    int frequency = Integer.parseInt(binding.frequency.getText().toString()) * 1000;
                    String contactName = binding.contactName.getText().toString();

                    binding.buttonFirst.setText("Parar");
                    binding.buttonFirst.setBackgroundColor(getResources().getColor(R.color.red_500));

                    binding.frequency.setEnabled(false);
                    binding.contactName.setEnabled(false);
                    binding.isVideo.setEnabled(false);

                    handler.postDelayed(new Runnable()
                    {
                        private long time = 0;

                        @Override
                        public void run()
                        {
                            // do stuff then
                            // can call h again after work!
                            time += frequency;
                            Log.d("TimerExample", "Going for... " + time);

                            String mime = "vnd.android.cursor.item/vnd.com.whatsapp.voip.call";
                            final boolean isVideo = binding.isVideo.isChecked();
                            if (isVideo) {
                                mime = "vnd.android.cursor.item/vnd.com.whatsapp.video.call";
                            }

                            makeCall(contactName, mime);
                            handler.postDelayed(this, frequency);
                        }
                    }, 1000); // 1 second delay (takes millis)

                } else {
                    binding.frequency.setEnabled(true);
                    binding.contactName.setEnabled(true);
                    binding.isVideo.setEnabled(true);

                    handler.removeMessages(0);
                    binding.buttonFirst.setText("Iniciar");
                    binding.buttonFirst.setBackgroundColor(getResources().getColor(R.color.purple_500));

                }
            }
        });
    }

    private void makeCall(String name, String mimeString) {
        String displayName = null;
        Long _id;
        ContentResolver resolver = getActivity().getApplicationContext().getContentResolver();
        Cursor cursor = resolver.query(ContactsContract.Data.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME);
        while (cursor.moveToNext()) {
            _id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data._ID));
            displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            String mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
            if (displayName.equals(name)) {
                if (mimeType.equals(mimeString)) {
                    String data = "content://com.android.contacts/data/" + _id;
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_VIEW);
                    sendIntent.setDataAndType(Uri.parse(data), mimeString);
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}